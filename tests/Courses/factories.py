import json
import factory
from faker import Faker
from django.conf import settings
from apps.Courses.models import Courses

faker = Faker('es_ES')

class CoursesFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Courses

    name = faker.name(max_nb_chars=30)
    slug = faker.slug()
    description = faker.text()
    seo_title =  faker.text(max_nb_chars=255)
    seo_meta_description = faker.text(max_nb_chars=255)
    amount_usd = faker.pydecimal(10, 2, positive=True)
    amount_pen = faker.pydecimal(10, 2, positive=True)
    final_project_description = faker.text()
    project_summary = faker.text()
    