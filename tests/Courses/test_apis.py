from unittest import skip
from django.test import TestCase
from django.contrib.auth.models import User  # pylint: disable=E5142
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
from tests.Courses.factories import CoursesFactory

class UpdateOrderStatusTest(TestCase):
    def setUp(self):
        self.courses = CoursesFactory()
        self.apiclient = APIClient()
        self.user = User.objects.create_superuser('admin', 'admin@example.com', 'secret')
        self.token = Token.objects.create(user=self.user)
        self.url = f'/api/v1/courses/course/'

    def test_not_access_without_token(self):
        resp = self.apiclient.post(self.url)
        self.assertEqual(resp.status_code, 403)

    def test_get_status_ok(self):
        self.apiclient.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token.key)
        resp = self.apiclient.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp['content-type'], 'application/json')


    def tearDown(self):
        self.courses.delete()
        self.token.delete()
        self.user.delete()
