from django.test import TestCase
from apps.Courses.models import Courses
from tests.Courses.factories import CoursesFactory


class CoursesTestCase(TestCase):
    def setUp(self):
        self.courses = CoursesFactory()
        self.field_list = [
            field.name for field in Courses._meta.get_fields()]

    def test_have_fields_needed_by_the_business(self):
        self.assertTrue('name' in self.field_list)
        self.assertTrue('slug' in self.field_list)
        self.assertTrue('description' in self.field_list)
        self.assertTrue('seo_title' in self.field_list)
        self.assertTrue('seo_meta_description' in self.field_list)
        self.assertTrue('amount_usd' in self.field_list)
        self.assertTrue('amount_pen' in self.field_list)
        self.assertTrue('final_project_description' in self.field_list)
        self.assertTrue('project_summary' in self.field_list)

    def test_method_str(self):
        self.assertEqual(str(self.courses.name), self.courier.__str__())

    def tearDown(self):
        self.courses.delete()
