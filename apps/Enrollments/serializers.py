from rest_framework import serializers
from .models import Enrollment

class EnrollmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Enrollment

        fields = ['id', 'group_id', 'course_id', 'student_id',]
        
        extra_kwargs = {
            'group_id': {'required': True},
            'course_id': {'required': True},
            'student_id': {'required': True},
        }
        read_only_fields = ['id']

    def create(self, validated_data):
        return Enrollment.objects.create(**validated_data)