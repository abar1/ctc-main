from rest_framework import viewsets
from .models import Enrollment
from .serializers import EnrollmentSerializer


class EnrollmentsViewSet(viewsets.ModelViewSet):
    queryset = Enrollment.objects.select_related().all()
    serializer_class = EnrollmentSerializer