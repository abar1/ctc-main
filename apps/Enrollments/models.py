from django.db import models
from apps.Courses.models import Courses
from apps.Groups.models import Group
from apps.Students.models import Students

# Create your models here.
class Enrollment(models.Model):
    group_id = models.ForeignKey(Group, on_delete=models.PROTECT)
    course_id = models.ForeignKey(Courses, on_delete=models.PROTECT)
    student_id = models.ForeignKey(Students, on_delete=models.PROTECT)

    def __str__(self):
        return f"{self.group_id}-{self.course_id}-{self.student_id}"