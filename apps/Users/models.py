from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from apps.General.models import DocType, Country
from apps.Users.managers import UserManager

role_choice = (
    ('p', "padre"),
    ('t', "tutor"),
)

class User(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField('first name', max_length=30)
    last_name = models.CharField('last name', max_length=150)
    email = models.EmailField('email address', unique=True)
    password = models.CharField("password", max_length=128, default='ctc2021')
    doc_type = models.ForeignKey(DocType, on_delete=models.PROTECT, blank=True, null=True)
    doc_number = models.CharField("Doc Number", max_length=30, blank=True, null=True)
    country = models.ForeignKey(Country, on_delete=models.PROTECT, null=True)
    role = models.CharField("role", choices=role_choice, null=True, max_length=1)
    is_staff = models.BooleanField("is_staff", default=False) # tmp
    
    USERNAME_FIELD = 'email'

    objects = UserManager()

    class Meta:
        verbose_name = ('user')
        verbose_name_plural = ('users')

    def __str__(self):
        return f"{self.first_name} {self.last_name}"