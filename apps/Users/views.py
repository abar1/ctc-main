import json
import pandas as pd
from rest_framework.response import Response
from rest_framework_simplejwt import views as jwt_views
from django.db import IntegrityError
from apps.Users.serializers import TestTokenObtainPairSerializer
from apps.Users.utils import create_data_enrollment, create_data_student, create_data_users
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser

class RegisterUsers(APIView):
    parser_classes = [MultiPartParser]

    def post(self, request, format=None):
        file = request.data['filename']
        exceptions={"duplicates":[], "incompletes":[]}
        if file:
            readxls = pd.read_excel(file, index_col=0)
            for key, row in readxls.iterrows():
                row_to_evaluate = row.isna()
                prohibited = False
                for data_row in row_to_evaluate:
                    if data_row:
                        prohibited = True
                        break
                if prohibited:
                    exceptions['incompletes'].append(row.to_json())
                else:
                    try:
                        user = create_data_users(self, key, row) 
                        challenge = False
                        if user:
                            student = create_data_student(self, challenge, user, row)
                            if student:
                               create_data_enrollment(self, student, row)
                    except IntegrityError as ex:
                        exceptions['duplicates'].append(row.to_json())
                        
            return Response(status=200, data=json.dumps(exceptions))
        else:
            return Response(status=500)


class TestTokenObtainPairView(jwt_views.TokenObtainPairView):
    serializer_class = TestTokenObtainPairSerializer
    token_obtain_pair = jwt_views.TokenObtainPairView.as_view()