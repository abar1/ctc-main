
from apps.Courses.models import Courses
from apps.Enrollments.serializers import EnrollmentSerializer
from apps.General.models import OS, Country, DocType
from apps.Groups.models import Group
from apps.Students.models import Students
from apps.Students.serializers import StudentSerializer
from apps.Users.models import User
from apps.Users.serializers import UserSerializer


def generate_student_mail(first_name: str, last_name: str) -> str:
    last_name = last_name.split(" ")
    return f"{first_name[0].lower()}.{last_name[0].lower()}@crackthecode.la"

def create_data_users(self, key: str, row) -> User:
    doc_type = DocType.objects.get(Name=row[4])
    country = Country.objects.get(alpha_2_code=row[3].upper())
    role = "p" if row[1].lower() == "tutor" else  "t"
    data = {'first_name':key, 'last_name':row[0], 'email':row[2], 'password':row[2], 'password2':row[2], 'doc_type':doc_type, 'doc_number':row[5], 'country':country, 'role':role}
    add_user = UserSerializer.create(self, validated_data=data)
    return add_user


def create_data_student(self, challenge: bool, user:User, row) -> Students:
    if row[10].lower() == "si":
        challenge = True
    operating_system = OS.objects.get(Name = row[9])
    data = {
        "first_name":row[6],
        "last_name":row[7],
        "birthday":row[8],
        "email":generate_student_mail(row[6], row[7]),
        "operative_system":operating_system,
        "challenge":challenge,
        "guardian_id":user,
    }
    add_student = StudentSerializer.create(self, validated_data=data)
    return add_student

def create_data_enrollment(self, student:Students, row):
    group_id = Group.objects.get(id=row[15])
    course_id = Courses.objects.get(slug=row[14])
    data = {
        "group_id": group_id,
        "course_id": course_id,
        "student_id": student,
    }
    EnrollmentSerializer.create(self, validated_data=data)