from rest_framework import serializers
from .models import User
from rest_framework.validators import UniqueValidator
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from django.contrib.auth.password_validation import validate_password



class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
        )
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)


    class Meta:
        model = User
        fields = [ 'id', 'first_name', 'last_name', 'email', 'password', 'doc_type', 'doc_number', 'country', 'role', 'is_staff', ]
        extra_kwargs = {
            'first_name': {'required': True},
            'last_name': {'required': True},
        }
    
    def passwd_validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            doc_type=validated_data['doc_type'],
            doc_number=validated_data['doc_number'],
            country=validated_data['country'],
            role=validated_data['role'],
        )

        user.set_password(validated_data['password'])
        user.save()

        return user

class TestTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        response = super().validate(attrs)
        response.update({'is_staff': self.user.is_staff})
        return response
