from django.urls import path
from apps.Users.views import RegisterUsers

urlpatterns = [
    path("registerUser/", RegisterUsers.as_view(), name='api_register_user'),
]