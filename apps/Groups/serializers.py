from rest_framework import serializers
from .models import Group

class GroupsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group

        fields = ['id', 'name', 'course_id', 'start_date', 'end_date', 'duration',]
        
        extra_kwargs = {
            'name': {'required': True},
            'course_id': {'required': True},
            'star_date': {'required': True},
            'end_date': {'required': True},
        }