from django.db import models
from apps.Courses.models import Courses

day_choice = (
    (0,"monday"),
    (1,"tuesday"),
    (2,"wednesday"),
    (3,"thursday"),
    (4,"friday"),
    (5,"saturday"),
    (6,"sunday"),
)

class Duration_Course(models.Model):
    day = models.IntegerField("dia", choices=day_choice)
    time = models.TimeField(auto_now=False, auto_now_add=False)

    def __str__(self):
        return f"{self.get_day_display()}-{self.time}"

class Group(models.Model):
    name = models.CharField('name', max_length=100)
    course_id = models.ForeignKey(Courses, on_delete=models.PROTECT)
    start_date = models.DateField("start date")
    end_date = models.DateField("end date")
    duration = models.ManyToManyField(Duration_Course)

    def __str__(self):
        return self.name