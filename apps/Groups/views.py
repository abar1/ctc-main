from rest_framework import viewsets
from .models import Group
from .serializers import GroupsSerializer


class GroupsViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupsSerializer