import logging
from django.core.management.base import BaseCommand
from django.core.management import call_command

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    def handle(self, *args, **options):
        call_command('migrate')
        self.stdout.write(self.style.SUCCESS('Start Migrate'))
        call_command('loaddata', 'apps/General/fixtures/Country.json')
        self.stdout.write(self.style.SUCCESS('Load fixture countrys'))
        call_command('loaddata', 'apps/General/fixtures/DocType.json')
        self.stdout.write(self.style.SUCCESS('Load fixture Docs Type'))
        call_command('loaddata', 'apps/General/fixtures/OS.json')
        self.stdout.write(self.style.SUCCESS('Load fixture systems'))
        call_command('loaddata', 'apps/Groups/fixtures/Duration_Course.json')
        self.stdout.write(self.style.SUCCESS('Load fixture Duration course'))
        call_command('loaddata', 'apps/Courses/fixtures/Courses')
        self.stdout.write(self.style.SUCCESS('Load fixture courses'))
        call_command('loaddata', 'apps/Groups/fixtures/Groups')
        self.stdout.write(self.style.SUCCESS('Load fixture groups'))
        self.stdout.write(self.style.SUCCESS('Successfully load fixtures'))
