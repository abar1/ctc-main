from rest_framework import serializers
from apps.General.models import DocType

class DocTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocType
        fields = '__all__'
