from django.contrib import admin
from .models import Country,DocType,OS

admin.site.register(DocType)
admin.site.register(Country)
admin.site.register(OS)