from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import DocTypeViewSet

router = DefaultRouter()
router.register('general', DocTypeViewSet, basename='generak')

urlpatterns = [
    path('', include(router.urls))
]