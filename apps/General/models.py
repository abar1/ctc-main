from django.db import models


class DocType(models.Model):
    Name = models.CharField("Name", max_length=21)

    class Meta:
        verbose_name = 'DocType'
        verbose_name_plural = 'DocType'
    
    def __str__(self):
        return self.Name

class Country(models.Model):
    Name = models.CharField("Name", max_length=100)
    alpha_2_code =  models.CharField("Alpha 2 code", max_length=2)

    class Meta:
        verbose_name = 'Country'
        verbose_name_plural = 'Country'
    
    def __str__(self):
        return self.Name

class OS(models.Model):
    Name = models.CharField("Name", max_length=100)

    class Meta:
        verbose_name = 'Operative System'
        verbose_name_plural = 'Operative System'

    def __str__(self):
        return self.Name