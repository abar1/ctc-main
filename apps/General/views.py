from drf_spectacular.utils import (
    OpenApiParameter,
    extend_schema,
    extend_schema_view,
)
from rest_framework import viewsets, mixins, permissions
from apps.General.models import DocType
from apps.General.serializers import DocTypeSerializer


@extend_schema_view(
    list=extend_schema(
        summary='List all doc types',
        description='Return a list of all master doc types in the system.',
        tags=['Doc Types'],
    ),
    retrieve=extend_schema(
        summary='Retrieve a doc types',
        description='Get details of a specific doc types',
        tags=['Doc Types'],
    ),
    create=extend_schema(
        summary='Create a doc types',
        description='Create a doc types',
        tags=['Doc Types'],
    )
)
class DocTypeViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin, mixins.RetrieveModelMixin, mixins.ListModelMixin):
    permission_classes = [permissions.IsAuthenticated]
    queryset = DocType.objects.all()
    serializer_class = DocTypeSerializer