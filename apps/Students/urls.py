from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import StrudentsViewSet

router = DefaultRouter()
router.register('student', StrudentsViewSet, basename='student')

urlpatterns = [
    path('', include(router.urls))
]