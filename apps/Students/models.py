from django.db import models
from django.conf import settings
from apps.General.models import OS

class Students(models.Model):
    first_name = models.CharField('first name', max_length=30)
    last_name = models.CharField('last name', max_length=150)
    birthday = models.DateField()
    email = models.EmailField('email address', max_length=256)
    operative_system = models.ForeignKey(OS, on_delete=models.PROTECT)
    challenge = models.BooleanField("challenge", default=True)
    guardian_id = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)

    class Meta:
        verbose_name = ('student')
        verbose_name_plural = ('students')

    def __str__(self):
        return f"{self.first_name} {self.last_name}"