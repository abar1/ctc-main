from rest_framework import viewsets
from .models import Students
from .serializers import StudentSerializer


class StrudentsViewSet(viewsets.ModelViewSet):
    queryset = Students.objects.all()
    serializer_class = StudentSerializer