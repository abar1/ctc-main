from rest_framework import serializers
from .models import Courses

class CoursesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Courses
        fields = ['id', 'name', 'slug', 'description', 'seo_title', 'seo_meta_description', 'amount_usd', 'amount_pen', 'final_project_description', 'project_summary',]
        
        extra_kwargs = {
            'name': {'required': True},
            'slug': {'required': True},
            'amount_usd': {'required': True},
        }