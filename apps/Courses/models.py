from django.db import models

class Courses(models.Model):
    name = models.CharField('name', max_length=30)
    slug = models.SlugField("slug", max_length=250)
    description = models.TextField("description")
    seo_title = models.CharField("seo title", max_length=255)
    seo_meta_description = models.CharField("seo meta description", max_length=255)
    amount_usd = models.DecimalField("amount usd", max_digits=10, decimal_places=2)
    amount_pen = models.DecimalField("amount pen", max_digits=10, decimal_places=2)
    final_project_description = models.TextField("final project description")
    project_summary = models.TextField("project summary")

    class Meta:
        verbose_name = ('courses')
        verbose_name_plural = ('courses')

    def __str__(self):
        return self.name