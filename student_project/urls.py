from django.contrib import admin
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework_simplejwt import views as jwt_views
from apps.Users.views import TestTokenObtainPairView
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)


API_URL = 'api/v1'

schema_view = get_schema_view(
    openapi.Info(
        title="CTC-TEST API",
        default_version='v1',
        description="",
        license=openapi.License(name="©2020 Crack The code. All Rights Reserved."),
    ),
    public=True,
    authentication_classes=(),
)

urlpatterns = [
    path(f'{API_URL}/docs/schema/', SpectacularAPIView.as_view(), name='schema'),
    path(f'{API_URL}/docs/swagger/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui',),
    path(f'{API_URL}/docs/redoc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc',),
    path('admin/', admin.site.urls),
    path(f'{API_URL}/token/', TestTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path(f'{API_URL}/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path(f'{API_URL}/users/', include("apps.Users.urls")),
    path(f'{API_URL}/students/', include("apps.Students.urls")),
    path(f'{API_URL}/courses/', include("apps.Courses.urls")),
    path(f'{API_URL}/groups/', include("apps.Groups.urls")),
    path(f'{API_URL}/general/', include("apps.General.urls")),
]
